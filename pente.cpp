#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include "Board.h"
#include "Pair.h"

using namespace std;

const string wooster[27] = {
"",
"",
"                          ww,",
"                         jQQ,  ||",
"                        ..]]QQQQ",
"                        ggQQQQQQ",
"   ..wwjjmmaaaa,        QQQQQQQQ",
"  j$$WWmmQQQQmmgg,      jjWWQQQQQQff",
" qPP-44QQQQWWQQWW66      jjQQQQQQQQQQ",
" 33  ]]WWWWQQQQQQQQp     jjQQQQQQQQWW,",
"j'' _!!yyQQQQQQQQWWff    =QQQQQQQQQQQQr",
"(   jQQQQQQQQQQQQmm _..qwwQQQQQQQQWWQQ=",
"    //jj44QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ(",
"   .=[[4444QQQQQQQQWWQQQQQQQQQQQQQQQQQQQQ((",
"     ''))]]QQQQQQQQQQQQQQQQQQQQQQQQQQQQWW'",
"      c=QQQQQQQQQQQQQQQQQQQQQQQQQQWWff",
"      ^-$$$$WWQQQQQQQQQQQQQQQQQQQQQQ''",
"       .))]]QQQQQQQQQQQQQQWWQQWWQQ^^",
"         ]QQQQQQQQQQQQQQQQBB??`",
"          44QQQQQQQQQQWWQQ'",
"          .44QQQQQQQQQQff",
"            44WWWW??QQff.",
"            ]]WW''.55[[" ,
"            ))^^   \\\\" ,
"            l    \",,",
"            22,,/  ..??\"\"''",
"            -"}; 

const string fograt[27] = {
"",
"",
"",
"		   g5,",
"                  iip[[   ],,",
"                 _44QQkk( 5aa44,",
"                 \"mmQQQQmmyymmff-`",
"             ..,, jwwQQFF\"\"YY!!''[7.",
"             ))FFXXWWPP^^ . ~~`))'.",
"             (w,, ,, .] __77",
"              )4p__ggmmddQQ66 .",
"               ]aaQQQQQQQQmmyyQQgg",
"               jjWWQQQQQQQQQQQQQQQQLL",
"              jjWWWWQQQQQQQQQQQQQQQQLL",
"             jjWWWWQQQQQQQQQQQQff\"\"'",
"            jQQWWQQQQQQQQQQQQWW'",
"           aammQQQQQQQQQQQQQQQQQQ",
"          jjWWWWQQQQQQQQQQQQQQQQPP",
"        -ss44TTQQQQQQQQQQQQQQQQWW[[",
"        jjWWQQZZ&&QQQQQQQQ@@$$QQ@@..",
"       jjWWccqyyQQWWQQBBPPqqQQWW'''.",
"     _yy55QQWWQQQQWW??kkwwWWQQWWff",
"   -ssmmmmQQQQee11wwaaQQQQQQQQWW''",
"  Q$$WWQQQQBB!!\"\"\"??44QQQQQQPP````",
"<%%QQQQQQQQ[[    `` )DD++",
"  ]99\?\?''       jQQp,,",
"             ]??\"\"??"};

int main(){

	char winner;
	string winner_name;
	Board the_board;
	the_board.show_board();
	cout << the_board.get_player_name() << "'s move." << endl;
	while(the_board.hasValidMove()){
		while(!(the_board.try_set(the_board.get_player()))){
			cout << "Invalid move. Please try again." << endl;
		}
		the_board.next_player();
		if((winner = the_board.has_five()) != ' '){
			if(winner == 'x'){
				winner_name = "WOOSTER";
			}else{
				winner_name = "FOGRAT";
			}
			cout << winner_name << " wins!" << endl;
			for(int i = 2; i < 27; i++){
				if(winner == 'x'){
					cout << wooster[i] << endl;
				}else{
					cout << fograt[i] << endl;
				}
			}
			break;
		}else{
			cout << the_board.get_player_name() << "'s move." << endl;
		}
		the_board.show_board();
		//cout << "Current 'winner' = '" << winner << "'" << endl;
	}
	return 0;

}

