#ifndef PAIR_H
#define PAIR_H
#include <iostream>
#include <iomanip>

using namespace std;

class Pair{
	public:
		Pair();
		Pair(const Pair&);
		void set_horiz(int);
		void set_vert(int);
		int get_horiz();
		int get_vert();
	private:
		int horiz, vert;
};

#endif // Pair.h
