#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>

#include "Pair.h"
#include "board.h"

using namespace std;

const string wooster[27] = {
"",
"",
"                          ww,",
"                         jQQ,  ||",
"                        ..]]QQQQ",
"                        ggQQQQQQ",
"   ..wwjjmmaaaa,        QQQQQQQQ",
"  j$$WWmmQQQQmmgg,      jjWWQQQQQQff",
" qPP-44QQQQWWQQWW66      jjQQQQQQQQQQ",
" 33  ]]WWWWQQQQQQQQp     jjQQQQQQQQWW,",
"j'' _!!yyQQQQQQQQWWff    =QQQQQQQQQQQQr",
"(   jQQQQQQQQQQQQmm _..qwwQQQQQQQQWWQQ=",
"    //jj44QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ(",
"   .=[[4444QQQQQQQQWWQQQQQQQQQQQQQQQQQQQQ((",
"     ''))]]QQQQQQQQQQQQQQQQQQQQQQQQQQQQWW'",
"      c=QQQQQQQQQQQQQQQQQQQQQQQQQQWWff",
"      ^-$$$$WWQQQQQQQQQQQQQQQQQQQQQQ''",
"       .))]]QQQQQQQQQQQQQQWWQQWWQQ^^",
"         ]QQQQQQQQQQQQQQQQBB??`",
"          44QQQQQQQQQQWWQQ'",
"          .44QQQQQQQQQQff",
"            44WWWW??QQff.",
"            ]]WW''.55[[" ,
"            ))^^   \\\\" ,
"            l    \",,",
"            22,,/  ..??\"\"''",
"            -"}; 

const string fograt[27] = {
"",
"",
"",
"     	    g5,",
"                  iip[[   ],,",
"                 _44QQkk( 5aa44,",
"                 \"mmQQQQmmyymmff-`",
"             ..,, jwwQQFF\"\"YY!!''[7.",
"             ))FFXXWWPP^^ . ~~`))'.",
"             (w,, ,, .] __77",
"              )4p__ggmmddQQ66 .",
"               ]aaQQQQQQQQmmyyQQgg",
"               jjWWQQQQQQQQQQQQQQQQLL",
"              jjWWWWQQQQQQQQQQQQQQQQLL",
"             jjWWWWQQQQQQQQQQQQff\"\"'",
"            jQQWWQQQQQQQQQQQQWW'",
"           aammQQQQQQQQQQQQQQQQQQ",
"          jjWWWWQQQQQQQQQQQQQQQQPP",
"        -ss44TTQQQQQQQQQQQQQQQQWW[[",
"        jjWWQQZZ&&QQQQQQQQ@@$$QQ@@..",
"       jjWWccqyyQQWWQQBBPPqqQQWW'''.",
"     _yy55QQWWQQQQWW??kkwwWWQQWWff",
"   -ssmmmmQQQQee11wwaaQQQQQQQQWW''",
"  Q$$WWQQQQBB!!\"\"\"??44QQQQQQPP````",
"<%%QQQQQQQQ[[    `` )DD++",
"  ]99\?\?''       jQQp,,",
"             ]??\"\"??"};

Board::Board():player1('x'), player2('o'), currplayer('x'), x_caps(0), o_caps(0){
        for(int i = 0; i < 9; i++){
                for(int j = 0; j < 9; j++){
                        player_marks[i][j] = ' ';
                }
        }
}

Board::Board(const Board& aBoard){
	curr_move = aBoard.curr_move;
        x_caps = aBoard.x_caps;
        o_caps = aBoard.o_caps;
        player1 = aBoard.player1;
        player2 = aBoard.player2;
        for(int i = 0; i < 9; i++){
                for(int j = 0; j < 9; j++){
                        player_marks[i][j] = aBoard.player_marks[i][j];
                }
        }
}

char Board::get_player(){
        return currplayer;
}

string Board::get_player_name(){
	if(currplayer == player1){
		return "WOOSTER";
	}else{
		return "FOGRAT";
	}
}

void Board::next_player(){
        if(currplayer == player1){
                currplayer = player2;
        }else{
                currplayer = player1;
        }
}

void Board::set(int a, int b, char player){
        player_marks[8 - b][a] = player;
	curr_move.set_horiz(a);
	curr_move.set_vert(b);
}

bool Board::try_set(char currplayer){
        int x, y;
        cout << "Enter a move (x,y): ";
        cin >> x >> y;
        cout << endl;
        x--; y--; // Board is 1-based, need to adjust.
        if(isValidMove(x, y)){
                set(x, y, currplayer);
                return true;
        }else{
                return false;
        }
}

bool Board::isValidPosition(int x, int y){
        if(x < 0 || x > 8 || y < 0 || y > 8){
                return false;
		  } else {
			  		 return true;
		  }
}

bool Board::isValidMove(int x, int y){
	     if(!isValidPosition(x, y)) {
			       return false;
        }else if(player_marks[8 - y][x] == ' '){
                return true;
        }else if(player_marks[8 - y][x] == 'x' || player_marks[8 - y][x] == 'o'){
                return false;
        }else{
                cout << "Something horrible happened and the board was corrupted. Exiting." << endl;
                exit(1);
        }
}

bool Board::hasValidMove(){
        bool valid = false;
        for(int i = 0; i < 9; i++){
                for(int j = 0; j < 9; j++){
                        if(player_marks[i][j] == ' '){
                                valid = true;
                        }
                }
        }
        return valid;
}

char Board::has_five(){
        for(int i = 0; i < 9; i++){
                for(int j = 0; j < 9; j++){
                        if(count5(i, 1, j, 1) || count5(i, 1, j, -1) || count5(i, -1, j, 1) || count5(i, -1, j, -1)){
                                return player_marks[i][j];
                        }
                        if(count5(i, 0, j, 1) || count5(i, 1, j, 0) || count5(i, 0, j, -1) || count5(i, -1, j, 0)){
                                return player_marks[i][j];
			}
                }
        }
        return ' ';
}

int Board::get_caps(char player){
	if(player == 'x'){
		return x_caps;
	}else{
		return o_caps;
	}
}

bool Board::count5(int r, int dr, int c, int dc) {
        char player = player_marks[r][c];  // remember the player.
        if(player == ' '){
                return false;
        }
        for (int i=1; i<5; i++){
			       int x = r + dr * i;
					 int y = c + dc * i;
                if(!isValidPosition(x, y)) {
                        return false;
                }
                if (player_marks[x][y] != player){
                         return false;
                }
        }
        return true;  // There were 5 in a row!
}

void Board::show_board(){
	const string* curr_art;
	if (currplayer == 'x'){
		curr_art = wooster;
	}else{
		curr_art = fograt;
	}
	int cols = 0;
        int rows = 10;

	cout << "  ____";
	for(int i = 1; i < 9; i++){
		cout<<"_____";
	}
	cout<< endl;

	for(int i = 0; i < 9; i++){
		cout << --rows;
		for(int j = 0; j < 9; j++){
			cout <<"|    ";
		}
		cout << "|" << curr_art[i * 3] <<  endl;
		cout << " ";

		for(int j = 0; j < 9; j++){
			cout <<"| " << player_marks[i][j] << "  ";
		}
		cout << "|" << curr_art[i * 3 + 1] <<  endl;
		cout << " ";
		for(int j = 0; j < 9; j++){
			cout <<"|____";
		}
		cout << "|" << curr_art[i * 3 + 2] << endl;
	}
	cout << " ";
	for(int i = 0; i < 9; i++){
		cout << "  " << cols + 1 << "  ";
		cols++;
	}
	cout << endl;
	cout << "x captures: " << get_caps('x') << endl;
	cout << "o captures: " << get_caps('o') << endl;

}
