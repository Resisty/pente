#ifndef BOARD_H
#define BOARD_H
#include <iostream>
#include <iomanip>
#include "Pair.h"

using namespace std;

class Board{
	public:
		Board();
		Board(const Board&);
		void show_board();
		bool try_set(char);
		bool hasValidMove();
		char get_player();
		string get_player_name();
		void next_player();
		char has_five();
	private:
		void set(int, int, char);
		bool isValidMove(int, int);
		bool isValidPosition(int, int);
		char player_marks[9][9];
		char player1, player2, currplayer;
		int x_caps, o_caps;
		bool count5(int, int, int, int);
		int get_caps(char);
		Pair curr_move;
};
#endif // Board.h
