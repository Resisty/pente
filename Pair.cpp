#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>

#include "Pair.h"

using namespace std;

Pair::Pair():vert(0), horiz(0){}

Pair::Pair(const Pair& aPair){
	vert = aPair.vert;
	horiz = aPair.horiz;

}

void Pair::set_horiz(int h){
	horiz = h;
}

void Pair::set_vert(int v){
	vert = v;
}

int Pair::get_horiz(){
	return horiz;
}

int Pair::get_vert(){
	return vert;
}
